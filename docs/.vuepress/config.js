module.exports = {
  title: 'Hello VuePress',
  description: 'Just playing around',
  base: '/vuepress-starter/',
  dest: 'public',
    themeConfig: {
      nav: [
        { text: 'External', link: 'https://google.com', target:'_self', rel:false },
        { text: 'Guide', link: '/guide/', target:'_blank' }
      ]
    }
  }
